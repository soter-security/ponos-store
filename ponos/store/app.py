"""
Module defining the ASGI app for the Ponos store.
"""

from fastapi import status, FastAPI, Depends, Request
from fastapi.exceptions import HTTPException
from fastapi.responses import Response

from .config import Configuration, default_config


app = FastAPI()


@app.get("/data/{key}", response_class = Response)
async def key_get(
    key: str,
    config: Configuration = Depends(default_config)
):
    """
    Return the data for the given key.
    """
    try:
        content_type, data = await config.backend.get(key)
    except KeyError:
        raise HTTPException(
            status_code = status.HTTP_404_NOT_FOUND,
            detail = f"No data for key '{key}'."
        )
    else:
        return Response(content = data, media_type = content_type)


@app.put(
    "/data/{key}",
    status_code = status.HTTP_204_NO_CONTENT,
    response_class = Response
)
async def key_put(
    key: str,
    request: Request,
    config: Configuration = Depends(default_config)
):
    """
    Set the data for the specified key.
    """
    content_type = request.headers['content-type']
    request_data = await request.body()
    await config.backend.put(key, content_type, request_data)
