"""
Module containing the base class for Ponos store backend implementations.
"""


class Backend:
    """
    Base class for Ponos store backends.
    """
    async def get(self, key: str) -> (str, bytes):
        """
        Return a (content type, data) tuple for the given key.

        If the key does not exist, ``KeyError`` should be raised.
        """
        raise NotImplementedError

    async def put(self, key: str, content_type: str, data: bytes):
        """
        Set the content type and data associated with a given key.
        """
        raise NotImplementedError
