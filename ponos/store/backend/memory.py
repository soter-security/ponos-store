"""
Module containing the memory store implementation.
"""

from typing import List

from pydantic import Field, constr

from .base import Backend as BaseBackend


class Backend(BaseBackend):
    """
    Store backend that stores keys and data in local memory.
    """
    def __init__(self):
        self._data = {}

    async def get(self, key):
        return self._data[key]

    async def put(self, key, content_type, data):
        self._data[key] = (content_type, data)
