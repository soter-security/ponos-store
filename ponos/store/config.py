"""
Module containing configuration objects.
"""

from importlib.metadata import entry_points
from typing import Any, Dict

from pydantic import Field, PrivateAttr, PydanticValueError, validator

from configomatic import ConfigurableObject

from .backend import Backend


ENTRY_POINT_NAME = 'ponos.store.backends'


class InvalidBackendError(PydanticValueError):
    """
    Raised when the given backend is not valid.
    """
    code = 'invalid_backend'
    msg_template = '"{backend_name}" is not a valid backend'


class Configuration(ConfigurableObject):
    """
    Configuration object for the Ponos store API.
    """
    class Config:
        default_path = '/etc/ponos/store.toml'
        path_env_var = 'PONOS_STORE_CONFIG'
        env_prefix = 'PONOS_STORE'

    #: The name of the backend to use
    #: This must correspond to one of the installed entry points
    backend_name: str = Field(..., alias = 'backend')
    #: The backend configurations, indexed by backend name
    backend_config: Dict[str, Dict[str, Any]] = Field({}, alias = 'backends')

    #: The instantiated backend
    _backend: Backend = PrivateAttr()

    @validator('backend_name')
    def backend_name_valid_entry_point(cls, v):
        """
        Validates that the backend type is a valid entry point name.
        """
        if any(ep.name == v for ep in entry_points()[ENTRY_POINT_NAME]):
            return v
        else:
            raise InvalidBackendError(backend_name = v)

    def __init__(self, **init_kwargs):
        super().__init__(**init_kwargs)
        # Assemble the backend with the given configuration
        eps = (ep for ep in entry_points()[ENTRY_POINT_NAME] if ep.name == self.backend_name)
        backend_cls = next(eps).load()
        init_kwargs = self.backend_config.get(self.backend_name, {})
        self._backend = backend_cls(**init_kwargs)

    @property
    def backend(self):
        """
        Returns the configured backend.
        """
        return self._backend


#: Default configuration instance
_default_config = Configuration()


def default_config():
    """
    Returns the default configuration instance.
    """
    return _default_config
